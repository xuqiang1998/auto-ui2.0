import { createRouter, createWebHashHistory } from 'vue-router'
import RootView from '../App.vue'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'root',
      redirect: '/home',
      component: RootView,
      children:[
        
      ]
    },
    {
      path: '/home',
      name: 'home',
      component: () => import('../views/ContentView.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/task',
      name: 'Task',
      component: () => import('../views/TaskManager.vue'),
      meta: { requiresAuth: true },
      redirect: '/test',
      children:[
        {
          path: '/test',
          name: 'Test',
          component: () => import('../views/test/TestManager.vue'),
          meta: { requiresAuth: true },
        },
        {
          path: '/test/stability',
          name: 'StabilityTest',
          component: () => import('../views/test/StabilityTest.vue'),
          meta: { requiresAuth: true },
        },
        {
          path: '/test/module',
          name: 'ModuleTest',
          component: () => import('../views/test/ModuleTest.vue'),
          meta: { requiresAuth: true },
        },
        {
          path: '/task/mine',
          name: 'TaskMine',
          component: () => import('../views/task/TaskMine.vue'),
          meta: { requiresAuth: true },
        },
        {
          path: '/task/report',
          name: 'TaskReport',
          component: () => import('../views/task/TaskReport.vue'),
          meta: { requiresAuth: true },
        },
        {
          path: '/data/mine',
          name: 'DataMine',
          component: () => import('../views/data/DataMine.vue'),
          meta: { requiresAuth: true },
        },
        {
          path: '/data/platform',
          name: 'DataPlatform',
          component: () => import('../views/data/DataPlatform.vue'),
          meta: { requiresAuth: true },
        },
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !localStorage.getItem('isAuthenticated')) {
    next('/login');
  } else {
    next();
  }
});

export default router
