// import './assets/main.css'
import 'ant-design-vue/dist/reset.css';
import { createApp } from 'vue';
import App from './App.vue';
import Antd from 'ant-design-vue';
import router from './router';
import { createStore } from 'vuex';

const store = createStore({
  state () {
    return {
      count: ['sub1'],
    }
  },
  mutations: {
    increment (state, payload) {
      state.count = payload;
    }
  }
})
const app = createApp(App)

app.use(router)
app.use(Antd)
app.use(store)

app.mount('#app')
