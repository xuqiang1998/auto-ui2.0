import axios from 'axios';

// 创建 Axios 实例
const service = axios.create({
  baseURL: 'http://localhost:8888/users', // 你的 API 地址
});

// 请求拦截器
service.interceptors.request.use(
  (config) => {
    // 在请求发送之前可以进行一些操作，比如添加请求头
    // 这里你可以添加需要的请求头，如 token
    // config.headers.common['Authorization'] = 'Bearer ' + yourToken;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// 响应拦截器
service.interceptors.response.use(
  (response) => {
    // 在收到响应后，你可以进行一些操作，比如处理响应数据
    // 这里你可以根据需要进行数据处理，例如数据转换、错误处理等
    return response.data;
  },
  (error) => {
    // 在响应出错时，可以进行错误处理
    // 这里你可以根据 HTTP 状态码等进行相应的错误处理
    return Promise.reject(error);
  }
);

export default service;
